﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desert_Bloom
{
    public class ModConfig
    {
        public bool PlayDesertTune { get; set; }

        public ModConfig()
        {
            PlayDesertTune = false;
        }

    }
}
